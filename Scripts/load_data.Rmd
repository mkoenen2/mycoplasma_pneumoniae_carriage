---
title: "DIMER load data"
author: "Mischa Koenen"
date: "`r Sys.Date()`"
output:
  html_document:
    number_sections: true
    toc: true
    highlight: tango
    theme: lumen
---

```{r setup, include=F, message=F}
# load packages
library("tidyverse")# includes ggplot2, tibble, tidyr, readr, purrr, dplyr, stringr, forcats
library(magrittr)
library(phyloseq)
library(here)
library(decontam)
library(readxl)
library(here)
library(tidyverse)
subdir_name <- "load_data"

#set paths
knitr::opts_knit$set(root.dir = here::here(), aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), fig.path=here("results", subdir_name, "figures/"), echo=FALSE, dpi=300)
theme_set(theme_bw()) # global option for ggplot2
```


# Load functions
```{r} 
source(here("src", "make_plots.R"))
```
# Load data

```{r,include=F}
seqtab <- readRDS(here("Data", "output", "seqtab.rds"))
taxa <- read.table(here("Data", "taxonomy", "assigntaxonomy.tsv"), stringsAsFactors = F) %>% as.matrix()
taxid <- read.table(here("Data", "taxonomy", "idtaxa.tsv"), stringsAsFactors = F) %>% as.matrix()
nreads <- read.table(here("Data", "stats", "Nreads.tsv"))

mapping<- read.delim(here("Data","metadata", "mapping_files", "output","mapping_overview.txt"))%>% dplyr::rename(c("Sample_ID" = "X.SampleID"))

Metadata_single<- readRDS(here("Data","metadata", "meta_single.Rda"))%>% .[, -2]
meta_all<-readRDS(here("Data","metadata", "output", "metadata_all.Rda"))
mapping_all<- left_join(mapping, meta_all)

#order
seqtab_all<-seqtab%>% subset(rownames(.)%in%mapping_all$Sample_ID)

seqtab_all[is.na(seqtab_all)]<-0

nrow(taxa)#5447 taxa
dim(seqtab_all)
nrow(mapping_all)#408 samples

seqtab_ordered <- seqtab_all[match(mapping_all$Sample_ID, rownames(seqtab_all)), ]
seqtab_ordered_RA <- t(t(seqtab_ordered)/colSums(seqtab_ordered))

#check
all(rownames(seqtab_ordered) == mapping_all$Sample_ID)
all(rownames(seqtab_ordered_RA) == mapping_all$Sample_ID)

mapping_all <- mapping_all  %>%  column_to_rownames("Sample_ID")
```

# Create Phyloseq
```{r}
ps_taxa <- create_phylo(seqtab_ordered, taxa, mapping_all)
```

# Prepare
```{r}
sample_data(ps_taxa)$Sampletype = factor(get_variable(ps_taxa, "type") %in% c("isoblank","pcrblank") )

ps_taxa_mock <- prune_samples(sample_data(ps_taxa)$type=="mock", ps_taxa)
ps_taxa_isoblank <- prune_samples(sample_data(ps_taxa)$type=="isoblank", ps_taxa)
ps_taxa_pcrblank <- prune_samples(sample_data(ps_taxa)$type=="pcrblank", ps_taxa)
ps_taxa_samples <- prune_samples(sample_data(ps_taxa)$type=="sample", ps_taxa)
ps_taxa_samples_sib <- prune_samples((sample_data(ps_taxa)$type=="sample"| sample_data(ps_taxa)$type=="sample_sib"), ps_taxa)

#summary(sample_data(ps_taxa_samples))
ps_samples_genus <- tax_glom(ps_taxa_samples, "genus")
taxa_names(ps_samples_genus) <- tax_table(ps_samples_genus)[,"genus"]
```

#Plot samples
```{r, ow='100%', w=10, h=10}
create_bar_ps(ps_taxa_samples, n_otus=15, hc_order=T)
create_hm_ps(ps_taxa_samples, n_otus=25, hc_order=T)
```

#Plot samples and blanks per run
```{r, ow='100%', w=10, h=10}
ps_taxa_samplesblanks <- prune_samples(sample_data(ps_taxa)$type %in% c("isoblank", "sample"), ps_taxa)
ncol(ps_taxa_samplesblanks@otu_table) #339 samples

ps_taxa_run14 <- prune_samples(sample_data(ps_taxa_samplesblanks)$run_nr=="14", ps_taxa_samplesblanks)
ps_taxa_run19 <- prune_samples(sample_data(ps_taxa_samplesblanks)$run_nr=="19", ps_taxa_samplesblanks)
ps_taxa_run25 <- prune_samples(sample_data(ps_taxa_samplesblanks)$run_nr=="25", ps_taxa_samplesblanks)
ps_taxa_run30 <- prune_samples(sample_data(ps_taxa_samplesblanks)$run_nr=="30", ps_taxa_samplesblanks)
ps_taxa_run33 <- prune_samples(sample_data(ps_taxa_samplesblanks)$run_nr=="33", ps_taxa_samplesblanks)

create_bar_ps(ps_taxa_run14, n_otus=20, hc_order=T) #Roseomonas in sample D364_1 not in blanks
create_bar_ps(ps_taxa_run19, n_otus=20, hc_order=T)
create_bar_ps(ps_taxa_run25, n_otus=20, hc_order=T)
create_bar_ps(ps_taxa_run30, n_otus=20, hc_order=T)
create_bar_ps(ps_taxa_run33, n_otus=20, hc_order=T)
```


# Plot mocks
```{r}
create_bar_ps(ps_taxa_mock, n_otus=15, hc_order = F) + theme(legend.position = "right") + guides(fill = guide_legend(ncol = 1))
```

# Plot blanks
```{r}
create_bar_ps(ps_taxa_isoblank, n_otus=25, hc_order = F) + theme(legend.position = "right") + guides(fill = guide_legend(ncol = 1))
```

#Decontam
## Examine data
```{r}
df_ps <- as.data.frame(sample_data(ps_taxa))
df_ps$LibrarySize <- sample_sums(ps_taxa)
df_ps <- df_ps[order(df_ps$LibrarySize),]
df_ps$Index <- seq(nrow(df_ps))

#Plot DNA size of each sample/blank/mock
ggplot(data=df_ps, aes(x=Index, y=LibrarySize, color=type)) + geom_point()
```


```{r}
## Remove samples with less than 10000 read from phyloseq object
dim(otu_table(ps_taxa_samples)) #4604 ASVs, 317 samples
min(sample_sums(ps_taxa_samples)) # lowest reads in sample is 9841

min(sample_sums(ps_taxa)) # No 0 read samples in dataset
dim(otu_table(ps_taxa)) #4604 ASVs, 340 samples/blanks/mocks
dim(otu_table(ps_taxa)) #5447 ASVs, 408 samples/blanks/mocks, including siblings

zeroreads<-prune_samples(sample_sums(ps_taxa)==0, ps_taxa) 
sample_data(zeroreads)$Sample_ID # 3 pcrblanks with 0 reads
ps_taxa <- prune_samples(sample_sums(ps_taxa)>=1, ps_taxa)#remove samples with 0 reads
ps_taxa<- prune_samples(sample_sums(ps_taxa)>=1, ps_taxa)#remove samples with 0 reads
dim(otu_table(ps_taxa))#5447 ASVs, 405 samples/blanks/mocks, including siblings

sample_data(ps_taxa)$quant_reading <- as.numeric(get_variable(ps_taxa, "qpcr_picogreen_ugul"))
sample_data(ps_taxa)$quant_reading <- as.numeric(get_variable(ps_taxa, "qpcr_picogreen_ugul"))

## remove pcrblank_19_4 since no Picogreen concentration in known
ps_taxa <- subset_samples(ps_taxa, sample_names(ps_taxa) != "pcrblank_19_4")
ps_taxa<- subset_samples(ps_taxa, sample_names(ps_taxa) != "pcrblank_19_4")
```

## Frequency method
```{r}
ncol(otu_table(ps_taxa))
ps_taxa <- ps_taxa%>% subset_samples(!is.na(qpcr_16S_pgul))#remove samples without qpcr_16s_pgul
ncol(otu_table(ps_taxa)) #340

ncol(otu_table(ps_taxa))
ps_taxa <- ps_taxa%>% subset_samples(!is.na(qpcr_16S_pgul))#remove samples without qpcr_16s_pgul

contamdf.freq<- isContaminant(ps_taxa, method="frequency", conc="qpcr_16S_pgul")

head(contamdf.freq) 
table(contamdf.freq$contaminant)#96 contanimant out of 4604 ASV's found by frequency method
head(which(contamdf.freq$contaminant)) # the 14, 16, 19, 24, 26, 31 (and more) ASV's are possible contaminants )

plot_frequency(ps_taxa, taxa_names(ps_taxa)[c(2,16)], conc="qpcr_16S_pgul") + xlab("16S Concentration (pg/ul)")
plot_frequency(ps_taxa, taxa_names(ps_taxa)[c(2,14)], conc="qpcr_16S_pgul") + xlab("16S Concentration (pg/ul)")

set.seed(100)
plot_frequency(ps_taxa, taxa_names(ps_taxa)[sample(which(contamdf.freq$contaminant),20)], conc="qpcr_16S_pgul") +  xlab("DNA Concentration (pg/ul)")
```
## Prevalence method
```{r}
sample_data(ps_taxa)$is.neg <- sample_data(ps_taxa)$type %in% c("isoblank", "pcrblank")
sample_data(ps_taxa)$is.neg <- sample_data(ps_taxa)$type %in% c("isoblank", "pcrblank")

contamdf.prev <- isContaminant(ps_taxa, method="prevalence", neg="is.neg")
table(contamdf.prev$contaminant) #314 contaminants in 4604 ASV's by prevalence method

head(which(contamdf.prev$contaminant)) # 16, 21, 24, 26, 31, 34, 38, 39, 41 (and more)

# Make phyloseq object of presence-absence in negative controls and true samples
ps.pa <- transform_sample_counts(ps_taxa, function(abund) 1*(abund>0))
ps.pa.neg <- prune_samples(sample_data(ps.pa)$Sampletype == "TRUE", ps.pa)
ps.pa.pos <- prune_samples(sample_data(ps.pa)$Sampletype == "FALSE", ps.pa)

# Make data.frame of prevalence in positive and negative samples
df.pa <- data.frame(pa.pos=taxa_sums(ps.pa.pos), pa.neg=taxa_sums(ps.pa.neg),
                      contaminant=contamdf.prev$contaminant)
ggplot(data=df.pa, aes(x=pa.neg, y=pa.pos, color=contaminant)) + geom_point() +
  xlab("Prevalence (Negative Controls)") + ylab("Prevalence (True Samples)")
```

## Either method
```{r}
# From Barplot RA ASVs in blanks: 20 most prevalent ASV in blanks are 14,16,19,21,24,26,31,34,38,40,41,43,46,48,52,54,57,58,76,80
contamdf.either <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = 0.1) #Threshold Default is 0.1. A length-two vector can be provided when using the either method: the first value is the threshold for the frequency test and the second for the prevalence test.
table(contamdf.either$contaminant) # 359 out of 4604 ASV's with either method
head(which(contamdf.either$contaminant), 15)  # 14, 16, 19, 21, 24, 26, 31, 34, 38, 39, 41, 43, 46, 52, 53 
contamdf.either.0102 <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = c(0.1,0.2))
table(contamdf.either.0102$contaminant) # 435 out of 4604 ASV's with either method
head(which(contamdf.either.0102$contaminant), 15) # 16, 19, 21, 24, 26, 31, 34, 38, 39, 40

contamdf.either.0202 <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = c(0.2,0.2))
table(contamdf.either.0202$contaminant) # 491 out of 4604 ASV's with either method
head(which(contamdf.either.0202$contaminant), 10) # 14, 16, 19, 21, 24, 26, 30, 31, 34, 38

contamdf.either.0303 <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = c(0.3,0.3))
table(contamdf.either.0303$contaminant) # 569 out of 4604 ASV's with either method
head(which(contamdf.either.0303$contaminant), 10) # 14, 16, 19, 21, 24, 26, 30, 31, 34

contamdf.either.0404 <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = c(0.4,0.4))
table(contamdf.either.0404$contaminant) # 648 out of 4799 ASV's with either method
head(which(contamdf.either.0404$contaminant), 10) # 7, 8, 10, 14, 16, 19, 21,23, 24, 26, removes important ASVs so threshold to0 high!

contams0102<- subset(contamdf.either.0102,contamdf.either.0102$contaminant==TRUE)
contams0202<- subset(contamdf.either.0202,contamdf.either.0202$contaminant==TRUE)
contams0303<- subset(contamdf.either.0303,contamdf.either.0303$contaminant==TRUE)
contams0404<- subset(contamdf.either.0404,contamdf.either.0404$contaminant==TRUE)

contamsdif0102_0202<-  contams0202%>% filter(!(rownames(contams0202) %in% rownames(contams0102)))
#Dolosigranulum_391 removed, but mostly extra contaminants in threshold 0.2/0.2 so continued

contamsdif0202_0303<-  contams0303%>% filter(!(rownames(contams0303) %in% rownames(contams0202)))
#Haemophilus_65 and Staphylococcus_526 removed, but mostly extra contaminants in threshold 0.3/0.3 so final 

contamsdif0303_0404<-  contams0404%>% filter(!(rownames(contams0404) %in% rownames(contams0303)))
#Haemophilus_65 and Staphylococcus_526 removed, but mostly extra contaminants in threshold 0.3/0.3 so final 

contamdf.all.either.0303 <- isContaminant(ps_taxa, method="either", neg="is.neg", conc = "qpcr_16S_pgul", threshold = c(0.3,0.3))
table(contamdf.all.either.0303$contaminant) # 569 out of 4604 ASV's with either method
head(which(contamdf.all.either.0303$contaminant), 10) # 14, 16, 19, 21, 24, 26, 30, 31, 34
```

## Removing contaminants from phyloseq object
```{r}
nrow(otu_table(ps_taxa)) #4604
ps.noncontam_either <-prune_taxa(!contamdf.either.0303$contaminant, ps_taxa)
nrow(otu_table(ps.noncontam_either)) #4035 (569 removed)

nrow(otu_table(ps_taxa)) #5447
ps.all_noncontam_either <-prune_taxa(!contamdf.all.either.0303$contaminant, ps_taxa)
nrow(otu_table(ps.all_noncontam_either)) #4799 (648 removed)

plot_frequency(ps_taxa, taxa_names(ps_taxa)[c(1,40)], conc="qpcr_16S_pgul") + xlab("16S Concentration (pg/ul)")
plot_frequency(ps_taxa, taxa_names(ps_taxa)[c(1,48)], conc="qpcr_16S_pgul") + xlab("16S Concentration (pg/ul)")

#remove Halomonas_40 en Halomonas_48
badTaxa<- c("Halomonas_40", "Halomonas_48")
goodTaxa <- setdiff(taxa_names(ps.noncontam_either), badTaxa)
ps.noncontam_either<-prune_taxa(goodTaxa,ps.noncontam_either)
nrow(otu_table(ps.noncontam_either)) #4033

goodTaxa.all <- setdiff(taxa_names(ps.all_noncontam_either), badTaxa)
ps.all_noncontam_either<-prune_taxa(goodTaxa.all,ps.all_noncontam_either)
nrow(otu_table(ps.all_noncontam_either)) #4797
```

# Removing ASVs that are not bacteria
```{r}
nrow(otu_table(ps.noncontam_either)) #4033 (470 removed)
ps_taxa_noncontam_either <- subset_taxa(ps.noncontam_either, family  != "Mitochondria" & class   != "Chloroplast" & domain !="Archaea") 
nrow(otu_table(ps_taxa_noncontam_either)) #3602 (431 removed)

#select only samples without blanks/mocks
ps_taxa_samples_noncontam_either <-prune_samples(sample_data(ps_taxa_noncontam_either)$type=="sample", ps_taxa_noncontam_either)
nrow(otu_table(ps_taxa_samples_noncontam_either)) #3602 
```

# Subramanian filter
```{r}
ps_RA <- transform_sample_counts(ps_taxa_samples_noncontam_either,function(otu) otu/sum(otu))

pres_abund_filter <- function(ps, pres=2, abund=0.001) { # Subramanian filter
  filter_taxa(ps, function(x) sum(x > abund)>=pres, TRUE) }

ps_filtered <- pres_abund_filter(ps_RA)
nrow(otu_table(ps_filtered)) #241 ASVs remaining in final set
```


#Output
```{r}
saveRDS(ps_taxa_samples_noncontam_either, file=here("Data/phyloseq.Rda"))
```

